#include "Configurations.h"

#include <fstream>
#include <iostream>
#include <stdexcept>
#include <algorithm>

const std::unordered_map<std::string, Configuration::Setting> Configuration::kStringToSetting =
{
	{"min_players", Configuration::Setting::MinimumPlayers},
	{"max_players", Configuration::Setting::MaximumPlayers},
	{"bot_hits_below", Configuration::Setting::BotHitsBelow},
};

const std::unordered_map<Configuration::Setting, std::string> Configuration::kSettingToString =
{
	{Configuration::Setting::MinimumPlayers, "min_players"},
	{Configuration::Setting::MaximumPlayers, "max_players"},
	{Configuration::Setting::BotHitsBelow, "bot_hits_below"}
};

Configuration::Configuration(const std::regex & validationRegex)
	: kValidationRegex(validationRegex)
{
}

void Configuration::Load(const std::string & filePath)
{
	std::ifstream inputFileStream(filePath);
	if (!inputFileStream)
		throw std::invalid_argument("The input file was not found or is corrupted.");

	std::string currentLine;
	while (std::getline(inputFileStream, currentLine))
	{
		std::smatch match;
		if (std::regex_search(currentLine, match, kValidationRegex))
		{
			auto setting = match[1];
			auto value = match[2];
			auto settingEnum = kStringToSetting.at(setting);

			m_configurationVariables[settingEnum] = std::stoi(value);
		}
	}
}

int Configuration::operator[](Setting setting) const
{
	auto it = m_configurationVariables.find(setting);
	if (it != std::end(m_configurationVariables))
	{
		return it->second;
	}
	throw  std::invalid_argument("The desired setting was not found.");
}

std::ostream & operator<<(std::ostream & os, const Configuration & configuration)
{
	std::for_each(std::cbegin(configuration.m_configurationVariables), std::cend(configuration.m_configurationVariables),
		[&os](const std::pair<Configuration::Setting, int>& pair)
	{
		auto settingToString = Configuration::kSettingToString.at(pair.first);
		os << settingToString << " " << pair.second << '\n';
	});

	return os;
}
