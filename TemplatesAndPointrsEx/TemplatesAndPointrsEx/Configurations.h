#pragma once

#include <unordered_map>
#include <unordered_set>
#include <regex>
#include <ostream>

class Configuration
{
public:
	enum class Setting
	{
		MinimumPlayers,
		MaximumPlayers,
		BotHitsBelow
	};
public:
	static const std::unordered_map<std::string, Setting> kStringToSetting;
	static const std::unordered_map<Setting, std::string> kSettingToString;
public:
	Configuration(const std::regex& validationRegex);

	void Load(const std::string& filePath);

	int operator[](Setting setting) const;

	friend std::ostream& operator << (std::ostream& os, const Configuration& configuration);
private:
	std::unordered_map<Setting, int> m_configurationVariables;
	const std::regex kValidationRegex;
};

