#pragma once

#include <iostream>

class TestData
{
public:
	static constexpr int kDefaultSize = 512;
	TestData()
	{
		_array = new int[kDefaultSize];
	}
	~TestData()
	{
		delete[] _array;
		std::cout << "Deleted object test data: " << this << '\n';
	}

	TestData(const TestData& other)
	{
		*this = other;
	}

	TestData& operator = (const TestData& other)
	{
		if (_array)
			delete[] _array;

		_array = new int[kDefaultSize];

		for (auto index = 0; index < kDefaultSize; ++index)
			_array[index] = other._array[index];

		return *this;
	}
private:
	int* _array;
};

