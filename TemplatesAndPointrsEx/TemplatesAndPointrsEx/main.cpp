#include "Any.h"
#include "Configurations.h"
#include "TestData.h"

#include <iostream>
int main()
{
	{
		Any a;
		TestData t;
		a = t;
	}
	Any a;
	double val = 2;
	a = val;
	val = 7;
	a = val;
	std::cout << "Test with double "
		<< ((a.get<double>() == val) ? 
			"PASSED" : "FAILED") 
		<< '\n';

	return 0;
}